<?php
$rockBands = [
    ['Beatles', 'Love Me Do', 'Hey Jude', 'Helter Skelter'],
    ['Rolling Stones', 'Waiting on a Friend', 'Angie', 'Yesterday\'s Papers'],
    ['Eagles', 'Life in the Fast Lane', 'Hotel California', 'Best of My Love'],
    ['Eagles', 'Life in the Fast Lane', 'Hotel California', 'Best of My Love'],
    ['Eagles', 'Life in the Fast Lane', 'Hotel California', 'Best of My Love'],
    ['Eagles', 'Life in the Fast Lane', 'Hotel California', 'Best of My Love'],
];
?>
<html>
<body>
    <table border="3">

    <?php foreach ($rockBands as $band) {?>
        <tr>
            <?php foreach ($band as $item) {?>
            <td><?php echo $item; ?></td>
            <?php }?>
        </tr>
    <?php }?>
    </table>
</body>
</html>
