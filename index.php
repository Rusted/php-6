<?php

$user = [
    'name' => 'Petras',
    'roles' => [
        'admin',
        'user',
    ],
];

$roles = $user['roles'];
$isAdmin = in_array('admin', $roles);

print_r($user);

print_r($roles);

var_dump($isAdmin);

$user = [
    'data' => [
        'name' => 'Petras',
        'roles' => [
            'admin',
            'user',
        ],
    ],
];

$roles = $user['data']['roles'];
$isAdmin = in_array('admin', $roles);

$a = 'test';
$b = 'test2';
$c = 'test3';
$data = [[$a]];

echo '<br>';
echo $data[0][0];

$data = [[[$a]]];

echo '<br>';
echo $data[0][0][0];

$data = [1, [1, 2, [1, 2, 3, $a]]];
echo '<br>';
echo $data[1][2][3];

$data = [1, [$a], [1, 2, [1, 2, $b, 4], 'apple' => [1, 2, 3, $c]]];

echo '<br>';
echo $data[1][0];

echo '<br>';
echo $data[2][2][2];

echo '<br>';
echo $data[2]['apple'][3];
