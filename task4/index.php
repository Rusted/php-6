<?php
require_once 'projects.php';

$checkedProjectsSum = 0;
if (isset($_GET['project_id'])) {
    foreach ($projects as $project) {
        if (in_array($project['id'], $_GET['project_id'])) {
            $checkedProjectsSum += $project['price'];
        }
    }

    echo "Sum of selected projects is $checkedProjectsSum<br>";
}

$yearSums = [];
foreach ($projects as $project) {
    $year = $project['year'];
    if (!isset($yearSums[$year])) {
        $yearSums[$year] = 0;
    }

    $yearSums[$year] += $project['price'];
}

ksort($yearSums);

?>

<html>
<form>
    <table>
        <tr>
            <th></th>
            <th>Sutrumpinimas</th>
            <th>Metai</th>
            <th>Programa</th>
            <th>Suma</th>
        </tr>
        <?php foreach ($projects as $project) {?>
        <tr>
            <td>
                <input type="checkbox" name="project_id[]" value="<?php echo $project['id']; ?>" <?php echo
    in_array($project['id'], $_GET['project_id']) ? 'checked' : '' ?> />
            </td>
            <td>
                <?php echo $project['short_name']; ?>
            </td>
            <td>
                <?php echo $project['year']; ?>
            </td>
            <td>
                <?php echo $project['program']; ?>
            </td>
            <td>
                <?php echo $project['price']; ?>
            </td>
        </tr>
        <?php }?>
    </table>
    <input type="submit" value="Skaičiuoti projektų sumą" />
</form>

<table>
    <tr>
        <?php foreach ($yearSums as $year => $yearSum) {?>
        <th>
            <?php echo $year; ?>
        </th>
        <?php }?>

    <tr>
        <?php foreach ($yearSums as $year => $yearSum) {?>
        <th>
            <?php echo $yearSum; ?>
        </th>
        <?php }?>
    </tr>
    </tr>
</table>

</html>